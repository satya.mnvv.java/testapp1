﻿FROM openjdk:8-jdk-alpine
RUN apk update && \
apk -Uuv add python py-pip && \
pip install awscli && \
apk --purge -v del py-pip && \
rm /var/cache/apk/*
VOLUME /tmp
EXPOSE 8080
ADD target/TestApp1.jar TestApp1.jar
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java  -jar /TestApp1.jar" ]