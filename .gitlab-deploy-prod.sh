#!/bin/bash

ssh -tt ec2-user@$PROD_DEPLOY_SERVER /bin/bash <<EOF
	sudo yum update -y
	sudo amazon-linux-extras install docker -y
	sudo service docker start 
	sudo usermod -a -G docker ec2-user 
	aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID 
	aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY 
	echo "connecting to aws ecr trying" 
	$aws_ecr_login
	echo "pulling docker image from ecr" 
	docker pull $REPOSITORY_URI:latest
	docker run -d -p 80:8080 $REPOSITORY_URI:latest
	echo 'done'
	exit 0
EOF

